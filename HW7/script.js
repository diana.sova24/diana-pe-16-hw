"use strict";

const myItems = [2, 4, "Pita", null, "45", "Steve", "Lorax", 66, 78];

///////////////////////////// LONG WAY RESOLVING ///////////////////////////

// const filterArray = (array, typeToExlude) => {
//   const filteredArray = array.filter((item) => typeof item !== typeToExlude);
//   return filteredArray;
// };

// console.log(filterArray(myItems, "string"));

//////////////////////////// SHORT WAY RESOLVING ///////////////////////////////

const filterArray = (array, typeToExlude) =>
  array.filter((item) => typeof item !== typeToExlude);

console.log(filterArray(myItems, "string"));
