const p = document.querySelectorAll("p");

const pBackground = p.forEach((p) => {
  p.style.backgroundColor = "#ff0000";
});

const idSearch = () => {
  const optionsList = document.getElementById("optionsList");
  const parent = optionsList.parentNode;
  const children = optionsList.childNodes;
  console.log(optionsList);
  console.log(parent);
  console.log(children);
};
idSearch();

const createNewParagraph = () => {
  const newElem = document.createElement("p");
  newElem.classList.add("testParagraph");
  const text = document.createTextNode("This is a paragraph");
  newElem.append(text);
  document.body.append(newElem);
};
createNewParagraph();

const header = document.querySelector(".main-header").children;
console.log(...header);
const headerChildren = [...header];
headerChildren.forEach((elem) => {
  elem.classList.add("nav-item");
});

const title = document.querySelectorAll(".section-title");
const searchedTitle = [...title];
searchedTitle.forEach((elem) => {
  elem.classList.remove("section-title");
});

// Код для завдань лежить в папці project.

// Знайти всі параграфи на сторінці та встановити колір фону #ff0000

// Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

// Встановіть в якості контента елемента з класом testParagraph наступний параграф -

// This is a paragraph

// Отримати елементи , вкладені в елемент із класом main-header і вивести їх у консоль.
//  Кожному з елементів присвоїти новий клас nav-item.
// Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
